
Here we report a brief description of the folders containing data
in the current directory.

===============

FOLDER:                                                                         ALIAS:
develop-v4.4.0-rev16810-hash29d3c00a8--results-at-Marconi-KNL-OpenMP_run1       OMP_run1  
develop-v4.4.0-rev16810-hash29d3c00a8--results-at-Marconi-KNL-OpenMP_run2       OMP_run2

develop-v4.4.0-rev16810-hash29d3c00a8--results-at-Marconi-KNL_t1                MPI_t1
develop-v4.4.0-rev16810-hash29d3c00a8--results-at-Marconi-KNL_t2                MPI_t2
develop-v4.4.0-rev16810-hash29d3c00a8--results-at-Marconi-KNL_t2_FIX            MPI_t2_FIX

===============

OMP scaling

* OMP_run1: is the folder containing the initial scaling study. It was run using
            HDF5 with parallel IO, which turned out to be problematic
            NGsBlkXp= 11            Ry      # [Xp] Response block size
            yambo pristine version

* OMP_run2: the main difference wrt OMP_run1 is that HDF5 I/O is now serial.
            NGsBlkXp= 09            Ry      # [Xp] Response block size
            The X cutoff has been lowered because not longer needed to be 
            larger to stress the parallel I/O
            yambo pristine version

MPI scaling

* MPI_t1:  initial runs. No OpenMP parallelism, nthreads=1; KNL nodes were not fully exploited
           (only 32 MPI task per node used). Parallelism was found to be suboptima in a 
           number of cases
           yambo pristine version
* MPI_t2:  similar tuns using nthreads=2, now properly filling the KNL nodes.
           yambo pristine version
           AF, AF2, AF22: are sets of calculations done by changing the parallel
           strucutre
           
* MPI_t2_FIX: multiple runs, labelled AF-FIX, AF-FIX2, AF-FIX3 using
           yambo version including init fix and modifying the input parameters
           for parallelism. Pls refer to the input file in each folder to see
           the differences.
           AF-FIX4  is the reference set of calculations used in the deliverable.
          
           
