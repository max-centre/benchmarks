#!/bin/bash
#SBATCH --job-name=test-qdot
#SBATCH --account=Pra19_MaX_1
#SBATCH --partition=m100_usr_prod
#SBATCH --output=mpi_%j.out 
#SBATCH --error=mpi_%j.err
#SBATCH --nodes=15
#SBATCH --ntasks-per-node=32
#SBATCH --ntasks-per-socket=16
#SBATCH --cpus-per-task=4
#SBATCH --time=00:29:00

# (NOTE Yambo-style bindings)
#
ml purge
ml gcc_env
ml siesta-max/1.0-14-elsi-gpu-no-elpa
#
date
which siesta
echo "-------------------"
#
export OMP_NUM_THREADS=1
## export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#
mpirun --map-by socket:PE=1 --rank-by core --report-bindings \
       -np ${SLURM_NTASKS}  \
       siesta covid.fdf

